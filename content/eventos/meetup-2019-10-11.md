+++
title = "Meetup 11 de Octubre 2019"
publishdate = "2019-09-14"
date = "2019-10-11"
event_place = "Tecnológico de Xalapa"
description = "El evento de octubre, por primera vez en el tec"
thumbnail = "/img/eventos/meetup-2019-10-11.jpg"
author = "categulario"
draft = false
turnout = 27
+++

## Las platicas

1. **Flask : simple y objetivo**, por Ivan Leon
2. **Entendiendo los smart contracts con Blockchain**, por Carlos Ricardo Ortiz Sordo
3. **Fundamentos de Django Webframework**, por Luis Enrique Berthet Dorantes
4. **Rudos vs Técnicos, los chivos expiatorios del mercado laboral**, por Fabiola Rodríguez Martínez

## Sobre la sede

El Tecnológico de Xalapa se encuentra al sur de la ciudad entrando por la avenida Arco Sur, que más arriba es rébsamen, sobre la calle Sin Nombre 405. Nos recibe por primera vez en la historia de la comunidad pero con los brazos abiertos en este nuevo evento.

[Mapa](https://www.google.com/maps/place/Instituto+Tecnol%C3%B3gico+Superior+de+Xalapa/@19.5049819,-96.8803193,16.75z/data=!4m5!3m4!1s0x85db324c8ce295c7:0x4da58d2adc774de0!8m2!3d19.5026488!4d-96.8801424)

## Sobre el evento

Los meetups de xalapacode son encuentros en los que se realizan una serie de desconferencias de 15 minutos de duración por miembros de la comunidad como tu sobre temas de ciencia, tecnología, cultura y sociedad. Estos eventos son gratuitos y libres. Al finalizar el evento no te olvides de saludar a los ponentes y a los asistentes para involucrate más en la comunidad.
