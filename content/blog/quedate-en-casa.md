+++
title = "Quedate en casa"
date = "2020-03-28"
draft = false
thumbnail = "/img/logo.png"
description = "Ayudemos a combatir la pandemia del Covid-19, quedemonos en casa si no es necesario"
markup = "mmark"
author = "Jail"
+++

Amigas y amigos miembros de xalapacode, estimados colaboradores y comunidades aliadas. Los abajo firmantes, organizadores de xalapacode, queremos hacer un llamado a que sigamos las indicaciones por parte de la Secretaría de Salud y del Gobierno de México y nos quedemos en casa sino tenemos razón alguna para salir. Quedate en casa.

Es momento también de combatir la desinformación y las falsas noticias que solo buscan generar el pánico y entorpecen las labores de nuestras autoridades sanitarias. Cada que compartimos una noticia no verificada estamos dañando la credibilidad y echando a perder el trabajo que estan haciendo nuestras hermanas y hermanos profesionales de la salud para aplanar la curva de contagio de esta pandemia.

Siempre hemos demostrado que somos solidarios ante la desgracia, que cuando nos tendemos la mano y apoyamos al que nos necesita salimos adelante. En esta ocasión lo volveremos a hacer. Vamos, entre todas y todos, a aplanar la curva. Vamos a salir de esta.

Por favor, quedate en casa.

Por ultimo, hacemos un llamado a todas las comunidades de tecnología del país a replicar el mensaje de las autoridades del gobierno federal. Sabemos que entre nosotros corre una ideología apolitica pero es momento de cerrar filas para hacerle frente a la crisis que se nos avecina.

Por favor, quedate en casa, quedemonos en casa.

Atentamente:

@jailandrade
@cesariux23
@categulario
@koffer
@0_Bitz
