+++
title = "Una lista de comandos de GIT. "
date = "2019-10-13"
draft = false
thumbnail = ""
description = "Git Rules"
author = "koffer"
author_uri = "/directorio/koffer"
markup = "mmark"
+++



Como parte del hacktoberfest de este 12 de Octubre, salio esta lista de comando de git. Son los comandos basicos para usar git.

Lo mejor es ver el documento en [github](https://github.com/XalapaCode/hacktoberfest/blob/master/git_tutorial.md)

De todas maneras esta es la lista:

Tutorial básico de git
Básicos
Saber el estado de las cosas

{{< highlight python >}}

>>> git status

{{< / highlight >}}


Saber cuántas ramas hay y en cuál estoy

{{< highlight python >}}
>>> git branch
{{< / highlight >}}

Cambios
Añadir cambios de archivos modificados

{{< highlight python >}}
>>> git add -p
{{< / highlight >}}

Añadir archivos nuevos

{{< highlight python >}}
>>> git add nombre-del-archivo
{{< / highlight >}}

Grabar en la historia los archivos añadidos con git add

{{< highlight python >}}
>>> git commit -m "mensaje de commit"
{{< / highlight >}}

Deshacer el último commit

{{< highlight python >}}
>>> git reset --soft HEAD~1
{{< / highlight >}}

si no se quieren dejar los cambios de ese commit cambiar --soft por --hard.
se puede cambiar el uno por otro número para deshacer más commits.
HEAD^ es equivalente a HEAD~1.
Ramas
Crea una nueva rama

{{< highlight python >}}
>>> git branch nombre-de-la-rama
{{< / highlight >}}

Cambiar de rama

{{< highlight python >}}
>>> git checkout nombre-de-la-rama
{{< / highlight >}}

Unir cambios de la rama mencionada a la rama en la que estamos

{{< highlight python >}}
>>> git merge nombre-de-la-rama-a-unir
{{< / highlight >}}

Repositorios remotos
Ver la lista de repositorios remotos asociados a este proyecto

{{< highlight python >}}
>>> git remote -v
{{< / highlight >}}

Añadir un nuevo repositorio remoto

{{< highlight python >}}
>>> git remote add nombre-del-demoto url-del-remoto
{{< / highlight >}}

Por ejemplo cuando contribuyes a un proyecto de código abierto es útil añadir un remoto de nombre upstream que tenga la URL del repositorio original (del que hiciste fork)
Crear una imagen de nuestra rama en el repositorio remoto (primer push)

{{< highlight python >}}
>>> git push -u origin nombre-de-la-rama
{{< / highlight >}}

Subir los cambios de la rama actual a su imagen remota

{{< highlight python >}}
>>> git push
{{< / highlight >}}

Bajar cambios del proyecto original

{{< highlight python >}}
>>> git pull upstream master
{{< / highlight >}}

Para esto hay que estar en la rama máster
si el comando falla quizá sea por que upstream no está definido, caso en el cual puedes usar el comando de esta lista para añadir un nuevo repositorio remoto (está más arriba).
Cosas lindas
Ver una lista bonita de cambios

{{< highlight python >}}
>>> git log --graph --decorate --oneline --all para ver una lista de cambios
{{< / highlight >}}
