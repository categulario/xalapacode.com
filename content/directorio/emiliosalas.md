+++
name = "Emilio Salas"
nickname = "emiliosalas"
description = "Desarrollador web y científico de datos que usa Python como lenguaje de programación principal"
skills = "Python, Flask, Django, JavaScript, Node.js, SQL, NoSQL, Gnu/Linux, REST, Distributed Systems, Data Science, IA"
thumbnail = "/img/directorio/emilio.jpg"
draft = false
date = "2020-11-13"
disponibility = "Full Remote"
rol = "miembro"
website = "https://www.facebook.com/EmilioSalas4585"
author = "emiliosalas"
+++

soy desarrollador web, científico de datos y capacitador online, imparto talleres de tecnologías mas demandas por las empresas e industria del desarrollo.

"You! You the people, have the power! The power to create machines! The power to create happiness!"
- Charlie Chaplin, at The Great Dictator (1940)
